# Open Policy Agent - First Steps

## How To

**Generate terraform plan and transform it to json :**  

```
terraform plan -out tfplan
terraform show -json tfplan > tfplan.json
```

**Example query on the plan with jq :**  

```
# List all resources types within the manifest (root and child modules)
cat tfplan.json | jq '[.resource_changes[].type] | unique'

# Select all s3 bucket and output their estimated names and tags after terraform applied
cat tfplan.json | jq '.resource_changes[] | select( .type == "aws_s3_bucket") | {"name":.change.after.bucket, "tags":.change.after.tags}'

# List all sources of modules called within the root module
cat tfplan.json | jq '[.configuration.root_module.module_calls[].source] | unique'
```

**Evaluate the plan :**  

```
opa eval --fail-defined --format pretty --data rules/ --input terraform/stacks/main/tfplan.json "data.main.errors[list]"
# return 0 (and output undefined) if no errors
# return 1 otherwise with some details on the errors on stdout
```

**Test OPA policies**  

```
opa test rules
opa test rules -v
opa test rules --explain full
```

## Usefull links

[www.openpolicyagent.org - Introduction](https://www.openpolicyagent.org/docs/latest/)  
[play.openpolicyagent.org - The Rego Playground](https://play.openpolicyagent.org/)  
[terraform.io - JSON Output Format](https://www.terraform.io/docs/internals/json-format.html)  
