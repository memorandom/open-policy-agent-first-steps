package global

get_all_managed_taggable_resources[resources] {
	resources := input.resource_changes[_]
    resources.mode == "managed"
    resources.change.after.tags
}
mandatory_tags_missing[msg] {
    mandatory_tags := {"project", "subproject", "owner", "env"}

    resource := get_all_managed_taggable_resources[_]
    existing_tags := {key | resource.change.after.tags[key]}
    missing_tags := mandatory_tags - existing_tags

    resource_address := resource.address
    resource_type := resource.type
    not count(missing_tags) == 0

    msg := sprintf("%s(%s) is missing tags : %s", [resource_type, resource_address, missing_tags])
}

get_modules[module_calls] {
	module_calls := input.configuration.root_module.module_calls[_]
}
module_source_is_invalid[msg] {
   module := get_modules[_]
   module_source := module.source
   not re_match(`^\.\.\/\.\.\/modules\/.+$`, module_source)
   msg := sprintf("module_calls(%s) source is not authorized", [module_source])
}

get_all_managed_resources[resources] {
	resources := input.resource_changes[_]
    resources.mode == "managed"
}
resource_type_is_forbidden[msg] {
	forbidden_types := ["aws_iam_role"]
	resources_types := get_all_managed_resources[_].type
    resources_types == forbidden_types[_]
	msg := sprintf("resource.type(%s) is blacklisted", [resources_types])
}

errors := list {
    list := mandatory_tags_missing | module_source_is_invalid | resource_type_is_forbidden
}
