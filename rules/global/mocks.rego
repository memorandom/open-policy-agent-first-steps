package global

invalid_tag_input = {
    "resource_changes": [
        {
            "address": "taggable_resource.foo",
            "mode": "managed",
            "type": "taggable_resource",
            "name": "foo",
            "change": {
                "after": {
                    "tags": {}
                }
            }
        },
        {
            "address": "taggable_resource.bar",
            "mode": "managed",
            "type": "taggable_resource",
            "name": "bar",
            "change": {
                "after": {
                    "tags": {
                        "env": "dev",
                        "subproject": "opa-first-steps"
                    }
                }
            }
        }
    ]
}

valid_tag_input = {
    "resource_changes": [
        {
            "address": "taggable_resource.foo",
            "mode": "managed",
            "type": "taggable_resource",
            "name": "foo",
            "change": {
                "after": {
                    "tags": {
                        "env": "dev",
                        "owner": "terraform",
                        "project": "memorandom",
                        "subproject": "opa-first-steps"
                    }
                }
            }
        },
        {
            "address": "untaggable_resource.bar",
            "mode": "managed",
            "type": "untaggable_resource",
            "name": "bar",
            "change": {
                "after": {}
            }
        }
    ]
}

invalid_module_input = {
    "configuration": {
        "root_module": {
            "module_calls": {
                "local_module": {
                    "source": "../../modules/local_module"
                },
                "git_module": {
                    "source": "git::https://mydomain.com/git_module"
                }
            }
        }
    }
}

valid_module_input = {
    "configuration": {
        "root_module": {
            "module_calls": {
                "foo": {
                    "source": "../../modules/foo"
                },
                "bar": {
                    "source": "../../modules/bar"
                }
            }
        }
    }
}

invalid_blacklist_type_input = {
    "resource_changes": [
        {
            "address": "aws_iam_role.foo",
            "mode": "managed",
            "type": "aws_iam_role",
            "name": "foo"
        },
        {
            "address": "aws_iam_role.bar",
            "mode": "foo",
            "type": "aws_iam_role",
            "name": "bar"
        },
        {
            "address": "allowed.x",
            "mode": "managed",
            "type": "allowed",
            "name": "x"
        }
    ]
}

valid_blacklist_type_input = {
    "resource_changes": [
        {
            "address": "aws_iam_role.bar",
            "mode": "foo",
            "type": "aws_iam_role",
            "name": "bar"
        },
        {
            "address": "allowed.x",
            "mode": "managed",
            "type": "allowed",
            "name": "x"
        }
    ]
}
