package global

test_rule_mandatory_tags_missing_pass {
    count(mandatory_tags_missing) == 0 with input as valid_tag_input
}
test_rule_mandatory_tags_missing_fail {
    count(mandatory_tags_missing) == 2 with input as invalid_tag_input

    mandatory_tags_missing[_] == "taggable_resource(taggable_resource.foo) is missing tags : {\"env\", \"owner\", \"project\", \"subproject\"}" with input as invalid_tag_input
    mandatory_tags_missing[_] == "taggable_resource(taggable_resource.bar) is missing tags : {\"owner\", \"project\"}" with input as invalid_tag_input
}

test_rule_module_source_is_invalid_pass {
    count(module_source_is_invalid) == 0 with input as valid_module_input
}
test_rule_module_source_is_invalid_fail {
    count(module_source_is_invalid) == 1 with input as invalid_module_input

    module_source_is_invalid[_] == "module_calls(git::https://mydomain.com/git_module) source is not authorized" with input as invalid_module_input
}

test_rule_resource_type_is_forbidden_pass {
    count(resource_type_is_forbidden) == 0 with input as valid_blacklist_type_input
}
test_rule_resource_type_is_forbidden_fail {
    count(resource_type_is_forbidden) == 1 with input as invalid_blacklist_type_input

    resource_type_is_forbidden[_] == "resource.type(aws_iam_role) is blacklisted" with input as invalid_blacklist_type_input
}
