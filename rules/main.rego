package main

import data.global as global
import data.s3 as s3
import data.lambda as lambda

errors := list {
    list := global.errors | s3.errors | lambda.errors
}
