package lambda

managed_lambdas[resources] {
	resources := input.resource_changes[_]
	resources.type == "aws_lambda_function"
	resources.mode == "managed"
}

lambda_name_is_invalid[msg] {
    lambda := managed_lambdas[_].change.after
    lambda_name := lambda.function_name
    not re_match(`^opa_first_steps-dev-(root|child)_lambda$`, lambda_name)
    msg := sprintf("lambda (%s) does not match naming convention", [lambda_name])
}

errors := list {
    list := lambda_name_is_invalid
}
