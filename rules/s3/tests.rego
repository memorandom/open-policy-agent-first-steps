package s3

test_rule_errors_fail {
	count(errors) != 0 with input as invalid_input
}

test_rule_errors_pass {
    count(errors) == 0 with input as valid_input
}

test_rule_name_is_invalid_fail {
    count(name_is_invalid) == 2 with input as invalid_input

	name_is_invalid[_] == "bucket(foo) does not match naming convention" with input as invalid_input
    name_is_invalid[_] == "bucket(bar) does not match naming convention" with input as invalid_input
}

test_rule_public_options_invalid_fail {
    count(public_options_invalid) == 6 with input as invalid_input

    public_options_invalid[_] == "aws_s3_bucket(foo) config is invalid : must have acl:private" with input as invalid_input
    public_options_invalid[_] == "aws_s3_bucket(foo) config is invalid : must have grant:[]" with input as invalid_input
    public_options_invalid[_] == "aws_s3_bucket(foo) config is invalid : must have policy:null" with input as invalid_input

    public_options_invalid[_] == "aws_s3_bucket(bar) config is invalid : must have acl:private" with input as invalid_input
    public_options_invalid[_] == "aws_s3_bucket(bar) config is invalid : must have grant:[]" with input as invalid_input
    public_options_invalid[_] == "aws_s3_bucket(bar) config is invalid : must have policy:null" with input as invalid_input
}
