package s3

managed_buckets[resources] {
	resources := input.resource_changes[_]
	resources.type == "aws_s3_bucket"
	resources.mode == "managed"
}

name_is_invalid[msg] {
    bucket := managed_buckets[_].change.after
    bucket_name := bucket.bucket
    not re_match(`^memorandom\..+[^.]\.[a-z]{3}\.root-bucket$`, bucket_name)
    msg := sprintf("bucket(%s) does not match naming convention", [bucket_name])
}

public_options_invalid[msg]{
    bucket := managed_buckets[_].change.after
    bucket.acl != "private"
    bucket_name := bucket.bucket
    msg := sprintf("aws_s3_bucket(%s) config is invalid : must have acl:private", [bucket_name])
}
public_options_invalid[msg]{
    bucket := managed_buckets[_].change.after
    bucket.grant != []
    bucket_name := bucket.bucket
    msg := sprintf("aws_s3_bucket(%s) config is invalid : must have grant:[]", [bucket_name])
}
public_options_invalid[msg]{
    bucket := managed_buckets[_].change.after
    bucket.policy != null
    bucket_name := bucket.bucket
    msg := sprintf("aws_s3_bucket(%s) config is invalid : must have policy:null", [bucket_name])
}

errors := list {
    list := name_is_invalid | public_options_invalid
}
