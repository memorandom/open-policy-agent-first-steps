package s3

invalid_input = {
    "resource_changes": [
        {
            "mode": "managed",
            "type": "aws_s3_bucket",
            "change": {
                "after": {
                    "bucket": "foo",
                    "acl": "foo",
                    "grant": ["foo"],
                    "policy": "foo"
                }
            }
        },
        {
            "mode": "managed",
            "type": "aws_s3_bucket",
            "change": {
                "after": {
                    "bucket": "bar",
                    "acl": "bar",
                    "grant": ["bar"],
                    "policy": "bar"
                }
            }
        }
    ]
}

valid_input = {
    "resource_changes": [
        {
            "mode": "managed",
            "type": "aws_s3_bucket",
            "change": {
                "after": {
                    "bucket": "memorandom.opa-first-steps.dev.root-bucket",
                    "acl": "private",
                    "grant": [],
                    "policy": null
                }
            }
        },
        {
            "mode": "foo",
            "type": "aws_s3_bucket",
            "change": {
                "after": {
                    "bucket": "foo",
                    "acl": "foo",
                    "grant": ["foo"],
                    "policy": "foo"
                }
            }
        }
    ]
}
