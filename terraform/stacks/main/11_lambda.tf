data "archive_file" "init" {
  type        = "zip"
  output_path = "${path.module}/init.zip"

  source {
    content  = "Dummy"
    filename = "init.py"
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "root_lambda" {
  filename      = data.archive_file.init.output_path
  function_name = var.root_lambda_name
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "none"
  runtime       = "python3.8"

  tags = var.root_lambda_tags
}

module "lambda" {
  source   = "../../modules/lambda"
  name     = var.module_lambda_name
  tags     = var.module_lambda_tags
  role_arn = aws_iam_role.iam_for_lambda.arn
  filename = data.archive_file.init.output_path
}
