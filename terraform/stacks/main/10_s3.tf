resource "aws_s3_bucket" "root_bucket" {
  bucket = var.root_bucket_name
  acl    = "private"
  tags   = var.root_bucket_tags
}

module "s3" {
  source = "../../modules/s3"
  name   = var.module_bucket_name
  tags   = var.module_bucket_tags
}
