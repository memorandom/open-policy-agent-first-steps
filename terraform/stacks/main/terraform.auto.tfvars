root_bucket_name = "memorandom.opa-first-steps.dev.root-bucket"
root_bucket_tags = {"project"="memorandom","subproject"="opa-first-steps","owner"="terraform","env"="dev"}

module_bucket_name = "memorandom.opa-first-steps.dev.child-bucket"
module_bucket_tags = {"project"="memorandom","subproject"="opa-first-steps","owner"="terraform","env"="dev"}

root_lambda_name = "opa_first_steps-dev-root_lambda"
root_lambda_tags = {"project"="memorandom","subproject"="opa-first-steps","owner"="terraform","env"="dev"}

module_lambda_name = "opa_first_steps-dev-child_lambda"
module_lambda_tags = {"project"="memorandom","subproject"="opa-first-steps","owner"="terraform","env"="dev"}
