variable root_bucket_name {
  type        = string
  description = "name of the bucket created in the root_module"
}
variable root_bucket_tags {
  type        = map
  description = "tags of the bucket created in the root_module"
}

variable module_bucket_name {
  type        = string
  description = "name of the bucket created in the child module"
}
variable module_bucket_tags {
  type        = map
  description = "tags of the bucket created in the child module"
}

variable root_lambda_name {
  type        = string
  description = "name of the lambda created in the root_module"
}
variable root_lambda_tags {
  type        = map
  description = "tags of the lambda created in the root_module"
}

variable module_lambda_name {
  type        = string
  description = "name of the lambda created in the child module"
}
variable module_lambda_tags {
  type        = map
  description = "tags of the lambda created in the child module"
}
