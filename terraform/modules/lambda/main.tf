resource "aws_lambda_function" "child_lambda" {
  filename      = var.filename
  function_name = var.name
  role          = var.role_arn
  handler       = "none"
  runtime       = "python3.8"

  tags = var.tags
}
