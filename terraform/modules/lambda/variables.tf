variable name {
  type        = string
  description = "lambda name"
}
variable tags {
  type        = map
  description = "lambda tags"
}
variable filename {
  type        = string
  description = "path to the function's deployment package within the local filesystem"
}
variable role_arn {
  type        = string
  description = "IAM role attached to the Lambda Function"
}
