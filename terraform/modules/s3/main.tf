resource "aws_s3_bucket" "child_bucket" {
  bucket = var.name
  acl    = "private"
  tags   = var.tags
}
resource "aws_s3_bucket_public_access_block" "block_root_bucket" {
  bucket = aws_s3_bucket.child_bucket.id

  block_public_acls   = true
  block_public_policy = true
}
